// console.log("Hello world!");

// [Section] JavaSript Synchronous vs Asynchronous

	// - JavaScript is by default synchronous means that only one statement can be executed at a time.

	console.log("Zawarudo!");


	// - Asynchronous means that we can proceed to execute other statments, while time consuming codes are running in the background

	// - Fetch API allows you to asynchronously request for a resources (data)
	// a "promise" is an object that represents the eventual completion (or failure) of an asynchronous  function and its resulting value
		/*
			syntax:
				fetch('url')
				.then((response) =>{})
		*/

		// - by using the .then method we can now check for the status of the promise.
		// - console.log(fetch('https://jsonplaceholder.typicode.com/posts').then((response) => console.log(response.status)))

		fetch('https://jsonplaceholder.typicode.com/posts').then((response) => console.log(response.status))

		// - the "fetch" method will return a "promise" that evolves to a "response" obj.
		// - the .then method captures the "Response" Obj and returns another "promise" w/c will eventually be "resolved" or "rejected".


	// * Retrieve Contents/Data from the Response Object.

		// fetch('https://jsonplaceholder.typicode.com/posts')
		// .then((response) => response.json())

		// using multiple "then" method creates a "promise chain"
			// .then((json) => console.log(json))

	// * Create a function that will demonstrate using 'async' and 'await' keywords.
		// - the 'async' and 'await' keywords is another approach that can be used to achieve an asynchronous code.
		// - used in functions to indicate w/c portion of the code should be waited for.
		// - creates an synchronous function


		async function fetchData(){
			// - waits for the "fetch" method to complete then stores the value in the result variable.

			let result = await fetch('https://jsonplaceholder.typicode.com/posts');

			console.log(result); // - result returned  by fetch is a 'promise'.

			console.log(typeof result); // returned "response" is an object.

			console.log(result.body); // we can't access the content of the "response" directly by accessing its body property.

			let json = await result.json(); // converts the data from the "response"
			console.log(json);
		}

		fetchData();


	// * Retrieve a Specific POST

		fetch('https://jsonplaceholder.typicode.com/posts/1')
		.then((response) => response.json())
		.then((json) => console.log(json))


	// * Create POST 

		/*
			syntax:
				fetch('URL', options)
				.then((response) => {})
				.then((response) => {})
		*/

		fetch('https://jsonplaceholder.typicode.com/posts',{

				method: 'POST', // - sets the method of the "Request" Obj to POST.
				headers: { // - specify that the content will be a JSON structure.
					'Content-Type':'application/json'
				},

				body: JSON.stringify({
					title: 'New Post',
					body: "Hello World",
					userId: 1
				}),
		})

		.then((response) => response.json())
		.then((json) => console.log(json))


	// * Updating a post using PUT Method

		// - PUT is used to update a single property
		fetch('https://jsonplaceholder.typicode.com/posts/1',{

			method: 'PUT',
			headers: {
				'Content-Type':'application/json'
			},

			body: JSON.stringify({
				title: 'Updated Post',
				body: 'Hello Again',
				userId: 1
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json))


	// * Updating a post using PATCH Method

		// - PATCH is used to update the whole object while
		fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'PATCH',
			headers: {
				'Content-Type':'application/json',
			},
			body: JSON.stringify({
				title: 'Corrected post'
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json))


	// * DELETING a post

		fetch('https://jsonplaceholder.typicode.com/posts/1',{
			
		})